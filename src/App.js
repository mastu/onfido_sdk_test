import React from "react";
import logo from "./logo.svg";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button } from "react-bootstrap";
import { init } from "onfido-sdk-ui";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        {/* <div id='onfido-mount'></div> */}
        <Button
          variant="primary"
          style={{ width: "300px", margin: "0.5em" }}
          onClick={() => initOnfido("country_id")}
        >
          Verify Country Id
        </Button>
        <Button
          variant="info"
          style={{ width: "300px", margin: "0.5em" }}
          onClick={() => initOnfido("passport")}
        >
          Verify Passport
        </Button>
      </header>
    </div>
  );
}

function initOnfido(mode) {
  console.log("Onfido started");

  var steps = [];

  if (mode === "country_id") {
    steps.push({
      type: "document",
      options: {
        documentTypes: {
          national_identity_card: true,
          passport: false,
          driving_licence: false
        },
        forceCrossDevice: true
      }
    });
    steps.push({
      type: "face",
      options: {
        uploadFallback: false,
        requestedVariant: "video"
      }
    });
  }

  if (mode === "passport") {
    steps.push({
      type: "document",
      options: {
        documentTypes: {
          national_identity_card: false,
          passport: true,
          driving_licence: false
        },
        forceCrossDevice: true
      }
    });
  }

  var onfidoOut = init({
    token:
      "eyJhbGciOiJIUzI1NiJ9.eyJwYXlsb2FkIjoib25RM1RnT2tYRVJIekV6T25zckl6SWtlSlppVlFOVlRHNUhpVjQwcHVKSjVUZm1TUHV5bzlLSmVYWldZXG5uT3dvNjZhY1pzN3Frb1d4VTYzRjFUK0dtaTJlWTRRREN1M2lWRFpLR3NkZTRCbz1cbiIsInV1aWQiOiJWbmQyc0J3YkVfTSIsImV4cCI6MTU2OTgzNzk5MSwidXJscyI6eyJvbmZpZG9fYXBpX3VybCI6Imh0dHBzOi8vYXBpLm9uZmlkby5jb20iLCJ0ZWxlcGhvbnlfdXJsIjoiaHR0cHM6Ly90ZWxlcGhvbnkub25maWRvLmNvbSIsImRldGVjdF9kb2N1bWVudF91cmwiOiJodHRwczovL3Nkay5vbmZpZG8uY29tIiwic3luY191cmwiOiJodHRwczovL3N5bmMub25maWRvLmNvbSIsImhvc3RlZF9zZGtfdXJsIjoiaHR0cHM6Ly9pZC5vbmZpZG8uY29tIn19.WAeF6vYgDMKnqMViWpPcrvbDqOSt80_yPQ8RztX2oWo",
    // containerId: 'onfido-mount',
    useModal: true,
    isModalOpen: true,
    onModalRequestClose: function() {
      onfidoOut.setOptions({ isModalOpen: false });
    },
    onComplete: function(data) {
      //todo commit data to backend here
      onfidoOut.setOptions({ isModalOpen: false });
      //* remove onfido from DOM this will allow to init it again on same page
      onfidoOut.tearDown();
      console.log("Onfido finished");
      console.log(data);
    },
    language: {
      locale: "ru",
      phrases: {
        cross_device: {
          intro: {
            document: {
              title: "Продолжить верификацию на мобильном телефоне",
              take_photos: "Мы покажем как сделать фото",
              action: "Продолжить"
            },
            face: {
              title: "Продолжить верификацию лица на мобильном телефоне",
              take_photos:
                "Пройдите верификацию лица чтобы подтвердить личность",
              action: "Продолжить"
            },
            sub_title: "Это легко, быстро и защищено.",
            sms: "Мы пришлем Вам SMS с линком (не нужно загружать приложение)",
            return_computer:
              "Вернитесь к компьютеру чтобы продолжить верификацию"
          }
        }
      }
    },
    smsNumberCountryCode: "RU",
    steps: steps
    //  steps: [
    //   {
    //     type:'welcome',
    //     options:{
    //       title:'Verify your document'
    //     }
    //   },
    //   {
    //     type: 'document',
    //     options: {
    //       documentTypes: {
    //         national_identity_card: true,
    //         passport: false,
    //         driving_licence: false
    //       },
    //       forceCrossDevice: true
    //     }
    //   },
    //   {
    //     type:'face',
    //     options: {
    //       uploadFallback: false,
    //       requestedVariant: 'video'
    //     }
    //   },
    //   {
    //      type: 'complete',
    //      options: {
    //        message: 'Thanks for uploading',
    //        submessage: 'You are all set now, proceed next step'
    //      }
    //   }
    //]
  });
}

export default App;
